<?php

use Symfony\Component\Debug\Debug;
use Symfony\Component\HttpFoundation\Request;

$env = defined('APPLICATION_ENV') ? APPLICATION_ENV :
    (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') :
        (isset($_SERVER['APPLICATION_ENV']) ? $_SERVER['APPLICATION_ENV'] : 'prod'));
$isProd = 'prod' == $env;

/** @var \Composer\Autoload\ClassLoader $loader */
$loader = require __DIR__ . '/../app/autoload.php';
if ($isProd) {
    include_once __DIR__ . '/../app/bootstrap.php.cache';
}else{
    Debug::enable();
}

$kernel = new AppKernel($env, !$isProd);
$kernel->loadClassCache();
//$kernel = new AppCache($kernel);

// When using the HttpCache, you need to call the method in your front controller instead of relying on the configuration parameter
//Request::enableHttpMethodParameterOverride();
$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
