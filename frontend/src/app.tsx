import * as QRCode from "qrcode.react";
import * as React from "react";
import * as Request from "superagent";
import * as URL from "url-parse";

export class AppComponent extends React.Component<void, State> {
	constructor() {
		super();
		this.state = {
			url: "",
			shortenedUrl: "",
			lastUrl: ""
		};
	}

	public render(): JSX.Element {
		return (
			<div>
				<div style={{ border: "1px #ccc solid", position: "relative" }}>
					<input
						style={this.state.url && !this.validatedUrl() ? { borderColor: "#e33" } : {}}
						className="url-input"
						placeholder="paste URL here ..."
						onChange={this.inputChange}
						onKeyDown={this.inputKeyPress}
					/>
					<button className="tinify-button" onClick={this.tinify}>tinify</button>
				</div>

				<div className={this.state.shortenedUrl ? "destination" : "destination collapsed"}>
					<input className="source-link" value={this.state.lastUrl} readOnly={true}/>
					<input className="destination-link" value={this.state.shortenedUrl} readOnly={true}/>
					<div className="qrcode"><QRCode value={this.state.shortenedUrl} size={29 * 6}/></div>
				</div>
			</div>
		);
	}

	private change(value: string) {
		this.setState({url: value});
	}

	private validatedUrl() {
		let rawUrl = this.state.url;
		let url = new URL(rawUrl, "http://");
		if (!url.host) return false;
		return url && url.toString();
	}

	private inputChange = (event: React.FormEvent<HTMLInputElement>) => {
		this.change(event.currentTarget.value);
	};

	private inputKeyPress = (event: React.KeyboardEvent<any>) => {
		if (event.key === "enter") {
			this.tinify();
		}
	};

	private tinify = () => {
		let base = "/shorten";
		let url = this.validatedUrl();
		if (!url)return;
		Request.post(base).send({url}).end((err, res) => {
			this.setState({shortenedUrl: res.body.link, lastUrl: url});
		});
	}
}

interface State {
	url?: string;
	shortenedUrl?: string;
	lastUrl?: string;
}
