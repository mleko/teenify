import * as React from "react";
import * as ReactDOM from "react-dom";

import {AppComponent} from "./app";

// Render a simple React component into the body.
let element = document.getElementById("app");
ReactDOM.render(<AppComponent/>, element);
