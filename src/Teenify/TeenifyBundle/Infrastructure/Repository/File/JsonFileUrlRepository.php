<?php
/**
 * @package teenify
 */


namespace Teenify\TeenifyBundle\Infrastructure\Repository\File;


use Teenify\TeenifyBundle\Entity\UrlEntry;
use Teenify\TeenifyBundle\Repository\UrlRepository;

class JsonFileUrlRepository implements UrlRepository
{

    /** @var string */
    private $filename;

    /** @var \ReflectionProperty */
    private $urlIdReflection;

    /**
     * FileUrlRepository constructor.
     * @param string $filename
     */
    public function __construct(string $filename)
    {
        $this->filename = $filename;
        $this->urlIdReflection = new \ReflectionProperty(UrlEntry::class, "urlId");
        $this->urlIdReflection->setAccessible(true);
    }

    /**
     * @inheritdoc
     */
    public function find(int $id): UrlEntry
    {
        $data = is_readable($this->filename) ? json_decode(file_get_contents($this->filename), true) : [];
        if (!array_key_exists($id, $data)) {
            return null;
        }
        $url = new UrlEntry($data[$id]);
        $this->urlIdReflection->setValue($url, $id);
        return $url;
    }

    /**
     * @inheritdoc
     */
    public function persist(UrlEntry $url)
    {
        $data = is_readable($this->filename) ? json_decode(file_get_contents($this->filename), true) : [];
        $key = array_search($url->getUrlId(), $data, true);
        if ($key) {
            $this->urlIdReflection->setValue($url, $key);
            return;
        }
        $max = (array_key_exists('max', $data) ? $data['max'] : pow(62, 5)) + 1;
        $data['max'] = $max;
        $data[$max] = $url->getUrl();
        file_put_contents($this->filename, json_encode($data));
        $this->urlIdReflection->setValue($url, $max);
    }
}
