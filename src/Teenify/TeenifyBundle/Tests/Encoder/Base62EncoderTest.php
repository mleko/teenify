<?php
/**
 * @package tinify.it
 */


namespace Tinify\Tests;


use PHPUnit\Framework\TestCase;
use Teenify\TeenifyBundle\Encoder\Base62Encoder;

class Base62EncoderTest extends TestCase
{
    public function testSymmetry()
    {
        $encoder = new Base62Encoder();

        $cases = [1234, 45665, 654345, 4234252352];

        foreach ($cases as $case) {
            $value = $encoder->decode($encoder->encode($case));
            $this->assertEquals($case, $value);
        }

        $this->assertEquals(0, $encoder->decode(''));
    }
}
