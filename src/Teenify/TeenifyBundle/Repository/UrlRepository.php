<?php
/**
 * @package teenify
 */


namespace Teenify\TeenifyBundle\Repository;


use Teenify\TeenifyBundle\Entity\UrlEntry;

interface UrlRepository
{
    /**
     * @param int $id
     * @return UrlEntry|null
     */
    public function find(int $id): UrlEntry;

    /**
     * @param UrlEntry $url
     * @return void
     */
    public function persist(UrlEntry $url);
}
