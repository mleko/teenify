<?php
/**
 * @package teenify
 */


namespace Teenify\TeenifyBundle\Encoder;


class Base62Encoder
{
    const CHARS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const BASE = 62;

    /**
     * @param int $int
     * @return string
     */
    public function encode(int $int): string
    {
        $int = (int)$int;
        $string = '';
        $chars = self::CHARS;
        while ($int) {
            $rest = $int % self::BASE;
            $int -= $rest;
            $int /= self::BASE;
            $string = $chars[$rest] . $string;
        }
        return $string;
    }

    /**
     * @param string $string
     * @return int
     */
    public function decode(string $string): int
    {
        $string = (string)$string;
        $value = 0;
        $length = strlen($string);
        for ($i = 0; $i < $length; $i++) {
            $pos = strpos(self::CHARS, $string[$i]);
            if (false === $pos) {
                return null;
            }
            $value = $value * self::BASE + $pos;
        }
        return $value;
    }
}
