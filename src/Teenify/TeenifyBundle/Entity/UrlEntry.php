<?php
/**
 * @package teenify
 */


namespace Teenify\TeenifyBundle\Entity;


class UrlEntry
{
    /** @var int */
    private $urlId;

    /** @var string */
    private $url;

    /**
     * ShortenedUrl constructor.
     * @param string $url
     */
    public function __construct(string $url)
    {
        $this->url = $url;
    }

    /**
     * @return int|null
     */
    public function getUrlId()
    {
        return $this->urlId;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

}
