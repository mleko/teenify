<?php
/**
 * @package teenify
 */


namespace Teenify\TeenifyBundle\Controller;


use Symfony\Component\HttpFoundation\Response;

class WelcomeController
{
    public function welcomeAction()
    {
        return new Response(file_get_contents(__DIR__ . "/../Resources/views/index.html"));
    }
}
