<?php
/**
 * @package teenify
 */


namespace Teenify\TeenifyBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Teenify\TeenifyBundle\Encoder\Base62Encoder;
use Teenify\TeenifyBundle\Entity\UrlEntry;

class UrlController extends Controller
{
    public function shortenAction(Request $request)
    {
        $data = new ParameterBag(json_decode($request->getContent(), true) ?: []);
        $url = $data->get("url");
        $urlEntry = new UrlEntry($url);

        $urlRepository = $this->get("teenify.repository.url_repository");
        $urlRepository->persist($urlEntry);

        $encoder = new Base62Encoder();
        $encodedId = $encoder->encode($urlEntry->getUrlId());

        $shortenedUrl = $this->get("router")->generate("teenify_expand", ["slug" => $encodedId], Router::ABSOLUTE_URL);

        return new JsonResponse(
            [
                'id'             => $urlEntry->getUrlId(),
                'link'           => $shortenedUrl,
                'url'            => $shortenedUrl,
                'destinationUrl' => $url
            ],
            201,
            [
                'Location' => $shortenedUrl
            ]
        );
    }

    public function expandAction($slug)
    {
        $encoder = new Base62Encoder();
        $id = $encoder->decode($slug);

        $urlRepository = $this->get("teenify.repository.url_repository");
        $url = $urlRepository->find($id);
        if ($url) {
            return new RedirectResponse($url->getUrl(), 301);
        }
        return new Response("Not found", 404);
    }
}
