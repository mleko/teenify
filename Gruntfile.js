module.exports = function(grunt) {
    require('jit-grunt')(grunt);

    grunt.initConfig({
        less: {
            development: {
                files: {
                    "web/css/style.css": "frontend/less/style.less"
                }
            }
        },
        postcss: {
            options: {
                map: {
                    inline: false
                },
                processors: [
                    require('autoprefixer')({
                        browsers: ['last 2 versions']
                    }),
                    require('cssnano')({
                        zindex: false
                    })
                ]
            },
            css_files: {
                src: [
                    "web/css/*.css"
                ]
            }
        },
        watch: {
            grunt: {
                files: [
                    'Gruntfile.js'
                ],
                tasks: ['default']
            },

            styles: {
                options: {
                    spawn: true
                },
                files: [
                    'asset/less/*.less'
                ],
                tasks: ['css']
            }
        }
    });
    grunt.registerTask('css', ['less', 'postcss']);

    grunt.registerTask('compile', ['css']);
    grunt.registerTask('default', ['compile', 'watch']);
};
